from pandas import DataFrame
import urllib.request
import logging
import datetime
from collections import OrderedDict

log = logging.getLogger(__name__)

URL_ROOT = "http://www.google.com/finance/getprices?"
URL_SEPERATOR = "&"
URL_INTERVAL = "i={interval}"
URL_DAYS = "p={days}d"
URL_BARS = "f=d,o,h,l,c,v"
URL_CPCT = "df=cpct"
URL_SYMBOL = "q={symbol}"
URL_FULL = URL_ROOT + URL_SEPERATOR.join([URL_INTERVAL, URL_DAYS, URL_BARS, URL_CPCT, URL_SYMBOL])

def convertColumnName(column):
    """ Converts a column name to a name more appropriate for the backtesting environment """
    if column == "DATE":
        return "datetime"
    else:
        return column.lower()

def read_infos(lines):
    """ Parses a given array of lines for information entries. Information entries are key-value
    pairs delimited with either an '=' or an '%3D'. The first line that does not contain one of
    these delimiters will be returned as the FIRST_DATA_LINE.

    Args:
        lines (list(str)): array of lines downloaded from google finance

    Yields:
        tuple: key-value pairs
    """
    count = 0
    for line in lines:
        fields = line.split("=")
        if len(fields) == 1:
            fields = line.split("%3D")
            if len(fields) == 1:
                yield ("FIRST_DATA_LINE", count)
                return
            elif len(fields) == 2:
                yield tuple(fields)
            else:
                raise ValueError("Expected 2-tuple; received {}".format(fields))
        elif len(fields) == 2:
            yield tuple(fields)
        else:
            raise ValueError("Expected 2-tuple; received {}".format(fields))
        count += 1

def soft_numeric(value):
    """ try to convert value to float or in, but just give value back unchanged if it doesn't work
    """
    try:
        return int(value)
    except ValueError:
        try:
            return float(value)
        except ValueError:
            return value

def read_data_lines(lines, columns, intervalInSeconds):
    """ Parses a given array of lines for data entries. Data entries are comma-separated values
    matching 1-to-1 to the names in the columns information entry.

    Args:
        lines (list(str)): array of lines containing data entries
        columns (list(str)): array of names to use as keys
        intervalInSeconds (int): interval of data in seconds

    Yields:
        dict: Dictionary of key-value pairs extracted from each line.
    """
    base_timestamp = None
    for line_num in range(len(lines)):
        line = lines[line_num]
        entry = {key: soft_numeric(value) for key, value in zip(columns, line.split(","))}
        entry_date = entry["datetime"]
        try:
            if entry_date.startswith("a"):
                base_timestamp = int(entry_date[1:])
                timestamp = base_timestamp
            else:
                log.warning("Non-fatal parse error on line {}: {}".format(line_num, line))
                continue
        except AttributeError:
            timestamp = base_timestamp + (entry_date * intervalInSeconds)

        entry["datetime"] = datetime.datetime.fromtimestamp(timestamp)
        entry["openinterest"] = 0
        yield entry

def get_google_data(symbol, interval, days):
    """ Downloads data for a given symbol and returns basic information and historical pricing.

    Args:
        symbol (str): symbol to look up
        interval (int): interval in minutes to look up
        days (int): number of days of data to download

    Returns
        tuple(dict, DataFrame): A tuple containing the dictionary of basic information and the
            DataFrame containing historical price data.
    """
    intervalInSeconds = interval * 60
    url = URL_FULL.format(symbol=symbol.upper(), interval=intervalInSeconds, days=days)
    log.debug("Loading URL: {}".format(url))

    with urllib.request.urlopen(url) as response:
        contents = response.read().decode('ascii')
        lines = contents.split("\n")
        infos = { key: value for key, value in read_infos(lines) }
        infos["INTERVAL"] = intervalInSeconds
        infos["COLUMNS"] = [convertColumnName(column) for column in infos["COLUMNS"].split(",")]
        log.info(infos)
        table = DataFrame(read_data_lines(
            lines[infos["FIRST_DATA_LINE"]:], infos["COLUMNS"], infos["INTERVAL"]))
        return (infos, table.set_index("datetime"))
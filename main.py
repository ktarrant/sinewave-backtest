import datetime
import pandas as pd

from backtrader import Cerebro, TimeFrame
from backtrader.sizers import PercentSizer
from backtrader.feeds.pandafeed import PandasData
from backtrader.indicators import MACD
from backtrader.analyzers import Returns

from supertrend import Supertrend
from standard import StandardIntradayStrategy
from util import load_cached_data

class BuyTheOpenStrategy(StandardIntradayStrategy):

    def __init__(self):
        super(BuyTheOpenStrategy, self).__init__()
        self.openTimer.plotinfo.plot = True

    def handle_open(self):
        """ Called when market opens """
        self.buy()

multiplier = 5
class MacdStrategy(StandardIntradayStrategy):
    params = (
        ('period_me1', 12 * multiplier),
        ('period_me2', 26 * multiplier),
        ('period_signal', 9 * multiplier),
    )

    def __init__(self):
        super(MacdStrategy, self).__init__()
        self.macd = MACD(period_me1=self.p.period_me1,
                         period_me2=self.p.period_me2,
                         period_signal=self.p.period_signal)

    def compute_factors(self):
        self.above_zero = self.macd.lines.macd[0] > 0
        self.cross_up = self.macd.lines.macd[0] > self.macd.lines.signal[0]

    def check_for_entry(self):
        return (self.above_zero and self.cross_up)

    def check_for_stop(self):
        return not self.above_zero

class SupertrendStrategy(StandardIntradayStrategy):
    params = (
        ('factor', 30),
        ('period', 70),
    )

    def __init__(self):
        super(SupertrendStrategy, self).__init__()
        self.st = Supertrend(factor=self.p.factor, period=self.p.period)

    def check_for_entry(self):
        return self.st.lines[0] == 1.0

    def check_for_stop(self):
        return self.st.lines[0] == -1.0

# Create a cerebro entity
cerebro = Cerebro()

# Set our desired cash start
cerebro.broker.setcash(100000.0)

# Set our sizer
cerebro.addsizer(PercentSizer, percents=90)
# cerebro.addsizer(MrTimerSizer)

# Load our data, create and add the datafeed
sample_table = load_cached_data(date=datetime.date.today(), symbol='TQQQ', interval=1, days=3)
datafeed = PandasData(dataname=sample_table, timeframe=TimeFrame.Minutes)
cerebro.adddata(datafeed)

# Add the strategies to run
# cerebro.addstrategy(SupertrendStrategy)
cerebro.optstrategy(SupertrendStrategy,
                    factor=range(1, 15, 2),
                    period=range(100, 700, 100))

# Add analyzers
cerebro.addanalyzer(Returns)

# Run the backtest
result = cerebro.run()

def process_returns(result):
    for opt_entry in result:
        for entry in opt_entry:
            for analyzer in entry.analyzers:
                analysis = analyzer.get_analysis()
                table = pd.Series(analysis)
                yield {
                    'factor': entry.p.factor,
                    'period': entry.p.period,
                    'return': table.loc['rtot'] * 100.0,
                }

returns = pd.DataFrame(process_returns(result))
returns.to_csv('returns.csv')

# Plot the result
# cerebro.plot()
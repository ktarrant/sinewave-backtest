from backtrader.indicators.basicops import OperationN

import numpy as np
import pandas as pd

class FourierProjection(OperationN):
    '''
    Calculates the highest value for the data in a given period
    Uses the built-in ``max`` for the calculation
    Formula:
      - highest = max(data, period)
    '''
    alias = ('FP',)
    lines = ('projection',)
    params = (
        ('period', 32),
        ('count', 5),
        ('offset', 16),
    )
    plotinfo = dict(subplot=False)

    def func(self, window):
        fft = pd.Series(np.fft.fft(window))
        mag = fft.abs().sort_values()
        fft[[i for i in mag.index[:-self.p.count]]] = 0
        approx = np.fft.ifft(fft)
        return approx[0]
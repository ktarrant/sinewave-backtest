from backtrader.strategy import Strategy

from intraday import OutputEventTimer

class StandardIntradayStrategy(Strategy):
    params = (
        ('open_offset', 30),
        ('close_offset', 30),
    )

    def __init__(self):
        self.openTimer = OutputEventTimer(offset=self.p.open_offset)
        self.openTimer.plotinfo.plot = False
        self.closeTimer = OutputEventTimer(base='close', offset=-self.p.close_offset)
        self.closeTimer.plotinfo.plot = False

    def tobps(self, value):
        return value / self.data0.close * 10000

    def nextstart(self):
        self.order = None
        self.next()

    def next(self):
        if self.order:
            # Already have pending order
            return

        self.compute_factors()

        if self.openTimer.lines.event[0]:
            self.handle_open()

        if not self.position:

            if self.openTimer.lines.phase[0] and not self.closeTimer.lines.phase[0]:

                if self.check_for_entry():
                    self.order = self.buy()
        else:

            if self.closeTimer.lines.event[0]:
                self.order = self.close()

            elif self.check_for_stop():
                self.order = self.close()

    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # The order was either completed or failed in some way
        self.order = None

    def handle_open(self):
        """ Called when the market is open """
        pass

    def compute_factors(self):
        """ Method for calculating intermediate factors """
        pass

    def check_for_entry(self):
        """ True if entry signal is triggered, else False """
        return False

    def check_for_stop(self):
        """ True if exit signal is triggered, else False """
        return False
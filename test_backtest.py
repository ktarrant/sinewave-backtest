import pytest
import logging
import datetime

from backtrader import Cerebro, TimeFrame
from backtrader.feeds.pandafeed import PandasData

from util import load_cached_data

log = logging.getLogger(__name__)

@pytest.fixture(scope="module")
def datafeed(request):
    # Use 1 minute data; Backtest between 5-15 days; Use a highly liquid ETF such as SPY, IWM, QQQ
    params = {'symbol': 'SPY', 'interval': 1, 'days': 5}
    sample_table = load_cached_data(date=datetime.date.today(), **params)
    print(sample_table)

    return PandasData(dataname=sample_table,
                      timeframe=TimeFrame.Minutes,
                      compression=params['interval'])

@pytest.fixture(scope="function")
def cerebro(request):
    # Create a cerebro entity
    cerebro = Cerebro()

    # Set our desired cash start
    cerebro.broker.setcash(100000.0)

    # Print out the starting conditions
    log.debug('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue( ))

    def fin():
        # Print out the final result
        log.debug('Final Portfolio Value: %.2f' % cerebro.broker.getvalue( ))

    request.addfinalizer(fin)

    return cerebro

@pytest.mark.dev
def test_empty_backtest(cerebro, datafeed):
    # Add the Data Feed to Cerebro
    cerebro.adddata(datafeed)

    # Run over everything
    cerebro.run()
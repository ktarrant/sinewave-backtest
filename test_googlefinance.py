import logging
import pytest
import time
import datetime

from googlefinance import get_google_data
from util import CACHE_FILE_FORMAT

log = logging.getLogger(__name__)

# These parameters should represent the limits that we can get from Google. Thanks Google!
# Some symbols return more incomplete data than others. DIA for example was not great.
@pytest.mark.parametrize("days", [1, 3, pytest.mark.slow(5), pytest.mark.slow(10), pytest.mark.slow(15)])
@pytest.mark.parametrize("interval", [pytest.mark.skip(1), 5, 15, 30])
@pytest.mark.parametrize("symbol", ["SPY", "QQQ", "IWM"])
def test_download(symbol, interval, days):
    expected_entries_per_day = 390 / interval + 1
    expected_entry_count = expected_entries_per_day * days

    start_time = time.clock()
    info, table = get_google_data(symbol, interval, days)
    duration = time.clock() - start_time
    log.debug(table)
    log.info("Loading {} days took {} seconds ({} seconds/day)".format(
        days, duration, duration/days))

    entry_count = len(table)
    print("Table contains {} entries, expected {} entries".format(
        entry_count, expected_entry_count))
    assert entry_count <= expected_entry_count

    dropped_samples = expected_entry_count - entry_count
    print("Table has {} dropped samples".format(dropped_samples))

    entries_per_day = entry_count / days
    print("That is {} entries per day, expected {} entries per day".format(
        entries_per_day, expected_entries_per_day))

    sample_percent = entries_per_day / expected_entries_per_day
    print("Table has {} % of samples".format(sample_percent * 100))
    assert sample_percent > 0.95

def test_save(symbol="SPY", interval=1, days=5):
    params = {'symbol':symbol, 'interval':interval, 'days':days}
    info, table = get_google_data(**params)
    filename = CACHE_FILE_FORMAT.format(date=str(datetime.date.today()), **params)
    table.to_csv(filename)
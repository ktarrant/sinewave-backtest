import pandas as pd
import logging

from backtrader import Strategy
from backtrader.indicator import Indicator
from backtrader.indicators import Highest, Lowest

from googlefinance import get_google_data

log = logging.getLogger(__name__)

CACHE_FILE_FORMAT = "{date}_{symbol}_{interval}m_{days}d.csv"

def load_cached_data(**params):
    cache_date = str(params.pop('date'))
    filename = CACHE_FILE_FORMAT.format(date=cache_date, **params)
    try:
        with open(filename, 'r') as fobj:
            sample_table = pd.read_csv(filename, parse_dates=['datetime']).set_index('datetime')
        log.info("Successfully loaded file: {}".format(filename))
    except FileNotFoundError:
        log.info("Downloading data: '{symbol}' {days} days @ {interval} interval".format(**params))
        sample_info, sample_table = get_google_data(**params)
        sample_table.to_csv(filename)
        log.info("Successfully saved file: {}".format(filename))
    return sample_table

class DonchianDifference(Indicator):
    params = (('period_fast', 1), ('period_slow', 5))
    lines = ('low','high')

    def __init__(self):
        self.lines.high = (Highest(period=self.p.period_fast) -
                           Highest(period=self.p.period_slow))
        self.lines.low = (Lowest(period=self.p.period_fast) -
                          Lowest(period=self.p.period_slow))